#include <iostream>

#include <SDL.h>

int main(int argc, char* argv[])

{

    if (SDL_Init(SDL_INIT_VIDEO) != 0)

    {

        std::cout << "Failed to initialize SDL." << std::endl;

        return 1;

    }

    std::cout << "Successfully initialized SDL!" << std::endl;

    system("pause");

    SDL_Quit();

    return 0;

}
