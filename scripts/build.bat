@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-09-09 16:58:55 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

:: Back to project root
cd ..

:: Build it
cmake -S . -B build
